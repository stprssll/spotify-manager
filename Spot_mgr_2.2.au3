#Region ;**** Directives created by AutoIt3Wrapper_GUI ****
#AutoIt3Wrapper_outfile=D:\Peter\My Autoit\Spot Manager\Spot_Mgr22.exe
#AutoIt3Wrapper_UseUpx=n
#EndRegion ;**** Directives created by AutoIt3Wrapper_GUI ****

#include <ButtonConstants.au3>
#include <EditConstants.au3>
#include <GUIConstantsEx.au3>
#include <WindowsConstants.au3>
#Region ### START Koda GUI section ### Form=d:\peter\my autoit\spot manager\spot_mgr.kxf
$AForm1 = GUICreate("Spot Manager 2.2", 356, 78, 332, 316)
$SilenceB = GUICtrlCreateButton("Silence", 8, 40, 81, 25)
$Display = GUICtrlCreateInput("", 8, 8, 337, 21)
GUICtrlSetState(-1, $GUI_DISABLE)
$UpB = GUICtrlCreateButton("^", 272, 40, 33, 25)
$DownB = GUICtrlCreateButton("v", 312, 40, 33, 25)
GUISetState(@SW_SHOW)
#EndRegion ### END Koda GUI section ###

Opt('MustDeclareVars', 1)

; INITIALISE

; Read control file - max 100 window titles
Dim $control[100]
Dim $i
Const $filename = ".\list.txt"
Dim $file
Dim $msg

Dim $begin	; timer
Dim $muted	; mute state
Dim $steps	; number of volume steps to return

Dim $win	; current window title
Dim $ret_hdl	; return window handle
Dim $ret_x	; return x,y corords
Dim $ret_y

Opt("MouseCoordMode", 0)        ;1=absolute, 0=relative, 2=client
Read_list()
$begin = TimerInit()
$muted = False
$steps = 0	; volume steps needed to return vol to prev

; MAIN CONTROL LOOP

While 1
	$msg = GuiGetMsg(1)
	Select
	Case $msg[0] = $GUI_EVENT_CLOSE
		ExitLoop

	Case $msg[0] = $SilenceB
		$file = FileOpen($filename, 1) ; Append
		FileWriteLine($file, $win)
		FileClose($file)
		Read_list()

	Case $msg[0] = $UpB
		VolumeUp()

	Case $msg[0] = $DownB
		VolumeDown()

	Case Else
		; Wake up every second to poll windows
		If TimerDiff($begin) > 1000 Then
			Proc_windows()
			$begin = TimerInit()
		EndIf
	EndSelect
WEnd

; END PROGRAM

Exit

;
; SUPPORT FUNCTIONS
;

Func Proc_windows()
	Dim $var
	Dim $win_count
	Dim $i
	Dim $tmpwin
	Dim $elt
	Dim $bad

	; Get window list
	$var = WinList("[REGEXPTITLE:Spotify.*]")

	; Process window list
	$win_count = $var[0][0]
	If $win_count = 1 Then

		; Just one Spotify window
		$win = $var[1][0]

		$tmpwin = $win
		If StringCompare(StringLeft($win, 10), "Spotify - ") = 0 Then
			; Trim the Spotify prefix to just leave the song name
			$tmpwin = StringTrimLeft($tmpwin, 10)
		EndIf

		Display($tmpwin)

		; When it just says "Spotify" there is an advert that is "on-hold"
		; Increase the volume until the name changes

		If $muted And $win = "Spotify" Then
			;inc volume
			VolumeUp()
			Return
		Else
			; Search banned titles for a match to this
			For $elt in $control
				; ! means a regular expression
				If StringLeft($elt, 1) = "!" Then
					$bad = (StringRegExp($win, StringRight($elt, StringLen($elt)-1)) = 1)
				Else
					$bad = (StringCompare($win, $elt) = 0)
				EndIf

				If $bad Then
					Display("[Mute]:" & $win)
					; Match found - beep warning then mute if not already
					If Not $muted Then
						Warn()
						Mute()
					EndIf
					Return
				EndIf
			Next

			; If you fall through here you are a regular song
			If $muted Then
				Warn()
				Unmute()
			EndIf
			Return
		EndIf
	EndIf

	If $win_count = 0 Then
		Display("* No Spotify Windows Active *")
	ElseIf $win_count > 1 Then
		Display("* Too many Spotify Windows Active *")
	EndIf

EndFunc

Func Read_list()
	$file = FileOpen($filename, 0) ; Read

	; Check if file opened for reading OK
	If $file = -1 Then
		; Don't care file will be created when "Silence" is pressed
		;MsgBox(0, "Error", "Unable to open list.txt.")
		Exit
	EndIf

	$i = 0
	; Read in lines of text until the EOF is reached
	While $i < 100
		$control[$i] = FileReadLine($file)
		$i = $i + 1
		If @error = -1 Then ExitLoop
		;MsgBox(0, "Line read:", $line)
	Wend
	FileClose($file)
EndFunc

Func Display($str)
	GUICtrlSetData($Display, $str)
EndFunc

Func Warn()
	For $i = 1 to 2
		Beep(500, 500)  ; 500Hz for 500Ms
		Sleep (100)
	Next

EndFunc

Func Mute()
	$steps = 0
	GetFocus()
	Do
		VolDown()
		$steps = $steps + 1
		$win = GetTitle()
	Until $win = "Spotify"
	VolUp()
	$muted = True
	RetFocus()
EndFunc

Func Unmute()
	GetFocus()
	For $i = 1 to $steps
		VolUp()
	Next
	$muted = False
	RetFocus()
EndFunc

Func VolumeUp()
	GetFocus()
	Volup()
	RetFocus()
EndFunc

Func VolumeDown()
	GetFocus()
	VolDown()
	RetFocus()
EndFunc

Func VolUp()
	Send("^{UP}")
	Sleep (100)
EndFunc

Func VolDown()
	Send("^{DOWN}")
	Sleep (100)
EndFunc

Func GetFocus()
	; get return window handle
	$ret_hdl = WinGetHandle("[ACTIVE]")

	$ret_x = MouseGetPos(0)
	$ret_y = MouseGetPos(1)

	WinActivate("[REGEXPTITLE:Spotify.*]")
	; need to click twice to trick Spotify,
	; but not so fast to trigger a double click
	;MouseClick("primary", 1, 1, 1, 0)
	;sleep(200)
	;MouseClick("primary", 3, 3, 1, 0)
EndFunc

Func RetFocus()
	WinActivate($ret_hdl)
	MouseMove($ret_x, $ret_y, 0)
EndFunc

Func GetTitle()
	Dim $var
	$var = WinList("[REGEXPTITLE:Spotify.*]")
	return $var[1][0]
EndFunc